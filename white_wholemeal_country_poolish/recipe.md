# White / wholemeal country loaf from poolish

Looking to hijack the best of the Tartine country loaf without using a starter.

## Percentages

* Flour **100%**
* Water **75%**
* Salt **2%**
* Dried yeast **0.4%**
* (Poolish **50%**)

## Ingredients - one loaf

### Poolish

* 125g white flour
* 125g wholemeal flour
* 250g water
* 0.4g yeast

### Dough

* 500g poolish
* 125g white flour
* 125g wholemeal flour
* 125g water
* 1.6g yeast
* 10g salt

## Process

### 1. Mix poolish

1. Mix flour and yeast dry in large bowl
1. Add water warm, mix by hand
1. Cover with towel and rest at room temperature for ~12h

### 2. Mix final dough

1. Mix flour, salt and yeast dry in large tub
1. Use warm water to wash poolish from its bowl into the tub, mix by hand

### 3. Bulk ferment and shape

1. Let the dough rest for ~3-4h, with a couple of folds in the tub every half hour for first 2h, then more like one an hour
1. Remove to bench, shape loosely and leave to rest for ~25m
1. Shape into rounds with envelope-style folds, move to proving basket / bowl seams-up
1. Prove for ~3h at room temp / overnight in fridge

### 4. Bake

1. Preheat oven to ~260C with casserole / dutch oven in (remove dough from fridge if fridge proved)
1. Remove pot from oven, upend dough into it, score and cover, return to oven
1. Immediately reduce temp to ~230C
1. Bake for 20m with lid on
1. Remove lid and bake for another 20-25m
1. Cool